import matplotlib
matplotlib.use('Agg')

import ast 
import pandas as pd
import numpy as np

from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.model_selection import train_test_split, KFold, ShuffleSplit, cross_val_score
from sklearn import linear_model
from sklearn.svm import SVR
from sklearn import metrics

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import scipy.stats as st


def show_most_informative_features(feature_names, nm, mdl, n):
    '''gives the top n features with the highest and lowest coefficients'''
    if nm == "SVR":
        coefs = mdl.coef_[0]
    else:
        coefs = mdl.coef_
    coefs_with_fns = sorted(zip(coefs, feature_names))
    top_high = coefs_with_fns[:n]
    top_low = reversed(coefs_with_fns[-n:])
    top = zip(top_high, top_low)
    print("\ncoefficient,word,coefficient,word")
    for (coef_1, fn_1), (coef_2, fn_2) in top:
        print("{0:.2f}\t{1}\t\t{2:.2f}\t{3}".format(coef_1, fn_1, coef_2, fn_2))

    for coef in coefs_with_fns:
        if coef[1] == "UH_SCORE":
            print(coef)

def save_plot(y_test, y_pred, name, ov_ob):
    '''creates and saves plots'''
    slope, intercept = np.polyfit(y_test, y_pred, 1)
    abline_values = [slope * i + intercept for i in y_test]

    colors = {"LR": "purple", "Ridge": "orangered", "SVR": "green"}
    color = colors[name]
    plt.scatter(y_test, y_pred, color=color, linewidth = 2)
    slope, intercept = np.polyfit(y_test, y_pred, 1)
    plt.plot(y_test, y_test*slope + intercept, 'r', color=colors[name], linewidth = 2)
    if name == "SVR":
        filename = "../results/plot" + ov_ob + ".png"
        patches = []
        for model in colors:
            data_key = mpatches.Patch(color=colors[model], label=model)
            patches.append(data_key)
        plt.legend(handles=patches)
        plt.xlabel("predicted percentage")
        plt.ylabel("true percentage")
        plt.savefig(filename)
        plt.clf()


with open("features.txt") as f:
    data = ast.literal_eval(f.read())
    
# remove municipalities from dict (<200 tweets)
rm_muns = ["Rozendaal", "het Bildt", "Baarle-Nassau", "Sint Anthonis", "Vaals", "Nuth", "Onderbanken", "Bergen (L.)", "Ten Boer", "Roerdalen", "Haarlemmerliede en Spaarnwoude", "Alphen-Chaam", "Simpelveld", "Ferwerderadiel", "Boekel"]
for mun in rm_muns:
    index = data["municipalities"].index(mun)
    data["tweets"].pop(index)
    data["ov"].pop(index)
    data["ob"].pop(index)
    data["scores"].pop(index)
    data["municipalities"].pop(index)

# get X (features)
vect = TfidfVectorizer()
tweets = vect.fit_transform(data["tweets"])
features = pd.DataFrame(tweets.toarray(), columns = vect.get_feature_names())
score_df = pd.Series(data["scores"])
features.insert(loc = 0, column = "UH_SCORE", value = score_df)
X = np.array(features)

# get y
y_ov = np.array(data["ov"])
y_ob = np.array(data["ob"])
ys = ("ov", y_ov), ("ob", y_ob)

models = [("LR", linear_model.LinearRegression()), ("Ridge", linear_model.Ridge()), ("SVR", SVR(kernel='linear'))]

# split corpus in train and test
number = 21
kf = KFold(n_splits = 10, shuffle = True, random_state = number)

for ovob, y in ys:
    count = 0
    for train_index, test_index in kf.split(X):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        count += 1

        for name, model in models:
            print("\n", name, ovob, count)
            
#            print(cross_val_score(model, X, y, cv=kf))
         
            # training and testing (predict) the model 
            model.fit(X_train, y_train)
            y_pred = model.predict(X_test)

            # evaluation
            print("\nexpl var score: ", metrics.explained_variance_score(y_test, y_pred))
            print("MAE: ", metrics.mean_absolute_error(y_test, y_pred))
            print("MSE: ", metrics.mean_squared_error(y_test, y_pred))
            print("Model score (R^2): ", model.score(X_test, y_test))
            print("\nnormality y_test: ", st.normaltest(y_test))
            print("normality y_pred: ", st.normaltest(y_pred))
            print("equal variances: ", st.levene(y_test, y_pred))
            print("equal variances: ", st.bartlett(y_test, y_pred))
            print("\nPearson R: ", st.pearsonr(y_test, y_pred))
            print("Spearman R: ", st.spearmanr(y_test, y_pred))

    #        print("Coefficients: ", model.coef_)
    #        print("Intercept: ", model.intercept_)
    
            show_most_informative_features(list(features), name, model, 25)
            save_plot(y_test, y_pred, name, ovob)

