# python3 feature.python3

# this file creates a dictionary in the following format:
# dict = {"municipalities": ["Amsterdam", ...], 
    # "tweets": ["This is a tweet", ...], 
    # "ov": [33.4, 35.2, ...], 
    # "ob": [14.2, 9.8, ...], 
    # "scores": [-0.0772, 0.0034, ...]}


from nltk.tokenize import TweetTokenizer


def get_wordlist(filename):
    '''returns a list with each line of a file as list element'''
    with open(filename) as f:
        return [word.rstrip() for word in f]

def clean_tweet(tweet, stopwords, wordlist, citmunlist):
    '''removes punctuation, symbols, usernames, stopwords and urls, and tokenizes a tweet'''
    tok_tweet = TweetTokenizer(strip_handles=True, preserve_case=False, reduce_len=True).tokenize(tweet)
    tok_tweet2 = []
    for word in tok_tweet:
        if word not in stopwords:
            for char in ["#","-","'"]:
                if char in word:
                    word = word.replace(char, "")
            if word.isalpha() == True:
                for name in citmunlist:
                    if name.lower() == word:
                        break
                if name.lower() == word:
                    break
                tok_tweet2.append(word)

    return tok_tweet2

def write_to_file(ds, filename):
    '''writes a datastructure to a file'''
    file = open(filename, "w")
    file.write(str(ds))
    file.close()


mun_tweets = {}

# create dictionary containing municipalities with all tweet text
stopwords = get_wordlist("stopwords.txt")
usernames = get_wordlist("users100.txt")
cmlist = get_wordlist("citmun_filter.txt")

with open("../tweets/tweets_all.txt") as f:
    tweets = [tweet for tweet in f.readlines()]

for line in tweets:
    tweet = line.rstrip().split("\t")
    if tweet[1] not in usernames:
        tweet_text = clean_tweet(tweet[0], stopwords, hu_words, cmlist)
        mun = tweet[2]
        if mun not in mun_tweets:
            mun_tweets[mun] = []
        mun_tweets[mun] += tweet_text

# make lists of the scores, overweight and obesity percentages
scores = []
ov_list = []
ob_list = []
with open("mun_scores.csv") as mun_scores:
    for line in mun_scores.readlines():
        line = line.strip().split(";")
        if line[0] in mun_tweets:
            scores.append(float(line[3]))
            ov_list.append(float(line[1]))
            ob_list.append(float(line[2]))

# convert the tweet text in the dict to a list
municipalities = []
tweet_text = []
ratio_list = []
for mun, text in sorted(mun_tweets.items()):
    municipalities.append(mun)
    tweet_text.append(" ".join(text))

# create a dictionary and save it to a file
feat_data = {}
feat_data["municipalities"] = municipalities
feat_data["tweets"] = tweet_text
feat_data["ov"] = ov_list
feat_data["ob"] = ob_list
feat_data["scores"] = scores

write_to_file(feat_data, "features.txt")

