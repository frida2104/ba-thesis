# command: 
#   python3 filter_all.py ../tweets/tweets_all.txt ../tweets/tweets_filteredt.txt

# this file:
#   tokenizes + cleans tweets
#   extracts all tweets containing health related words
#   labels the extracted tweets with a score based on those words
#   changes the location to municipality
#   writes the unfiltered tweets of argv[1] to argv[2]


import sys
import nltk
from nltk.tokenize import TweetTokenizer

 
def create_file_list(filename):
    '''creates a list of all lines in a file'''
    with open(filename, "r") as f:
        return [line.rstrip() for line in f.readlines()]

def create_tweet_list(filename):
    '''creates a list of all tweets in a file, without retweets'''
    with open(filename, "r") as f:
        return [line.rstrip() for line in f.readlines() if line.startswith("RT ") == False]

def clean_tweet(tweet):
    '''removes punctuation, symbols, usernames and urls, and tokenizes a tweet'''
    tok_tweet = TweetTokenizer(strip_handles=True, preserve_case=False, reduce_len=True).tokenize(tweet)
    cl_tweet = []
    for word in tok_tweet:
        if word.startswith("#"):
            word = word[1:]
        if word.isalpha() == True:
            cl_tweet.append(word)
        else:
            if "'" in word or "-" in word:
                cl_tweet.append(word)
    return cl_tweet

def label_tweet(tweet, words):
    '''returns a score based on the number of U, H or neutral words in the tweet'''
    score = 0
    filter = True
    for word in words:
        if word.split()[0] in tweet:
            filter = False
            if word.split()[1] == "U":
                score += 1
            elif word.split()[1] == "H":
                score -= 1
    if filter == False:
        return str(score)
    else:
        return False

def loc_to_mun(location, alist):
    '''returns the municipality belonging to the location'''
    if location == "nederland" \
    or location == "the netherlands" \
    or "belgie" in location \
    or "belgië" in location \
    or "belgium" in location \
    or len(location) < 2:
        return False
    loc = location.split(",")
    for cit_mun in alist:
        city = cit_mun.split(";")[0].lower()
        municipality = cit_mun.split(";")[1]
        mun = municipality.lower()
        if loc[0] == mun or loc[0] == city:
            return municipality
        if len(loc) > 1:
            if loc[1] == mun or loc[1] == city:
                return municipality
        if len(loc[0]) > 0:
            loc2 = loc[0].split()[0]
            if loc2 != "De" or loc2 != "Den":
                if loc2 == mun or loc2 == city:
                    return municipality
    return False
    
def write_to_file(list, filename):
    '''writes a list to file, placing each list element on a newline'''
    file = open(filename, "w")
    file.write("\n".join(list))
    file.close()
 
 
def main():
    words = create_file_list("words.txt")
    cit_mun_list = create_file_list("cit_mun.txt")
    tweets = create_tweet_list(sys.argv[1])

    filtered_tweets = []

    for tweet in tweets:
        tweet_words = tweet.split("\t")
        if len(tweet_words) == 3:
            tweet_text = clean_tweet(tweet_words[0])
            lw_tweet = [word.lower() for word in tweet_text]
            label = label_tweet(lw_tweet, words)
            if label != False:
                tweet_words.append(label)
                
                mun = loc_to_mun(tweet_words[2].lower(), cit_mun_list)
                if mun != False:
                    tweet_words[2] = mun
                    
                    tw = "\t".join(tweet_words)
                    print(tw)
                    filtered_tweets.append(tw)

    write_to_file(filtered_tweets, sys.argv[2])
    
if __name__ == "__main__":
    main()
