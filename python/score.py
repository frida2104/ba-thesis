# python3 score.py > mun_scores.csv

# this file appends health scores to the health statistics for each municipality

# output:
# municipality;ov%;ob%;score
# 's-Gravenhage;33.8;13.3;0.008073611727730412
# ...


def create_list(filename):
    with open(filename) as f:
        return [line.rstrip() for line in f.readlines()]

def average(dictionary):
    for key, counts in dictionary.items():
        dictionary[key].append(counts[1]/counts[2])
    return dictionary


dlt_users = create_list("users100.txt")
tweets = create_list("../tweets/tweets_all.txt")
usercount = {}
for tweet in tweets:
    split_tweet = tweet.split("\t")
    user, municipality, score = split_tweet[1], split_tweet[2], int(split_tweet[3])
    if user not in dlt_users:
        if user not in usercount:
            usercount[user] = [0, 0, municipality]
        usercount[user][0] += score
        usercount[user][1] += 1
    
for user, counts in usercount.items():
    usercount[user].append(counts[0]/counts[1])

mun_scores = {}
for user, counts in usercount.items():
    mun = counts[2]
    if mun not in mun_scores:
        mun_scores[mun] = [0,0]
    mun_scores[mun][0] += counts[3]
    mun_scores[mun][1] += 1

total = 0
for mun, counts in mun_scores.items():
    average = counts[0]/counts[1]
    mun_scores[mun].append(average)
    total += average

mun_average = total/len(mun_scores)

for mun, counts in mun_scores.items():
    centered_avg = counts[2] - mun_average
    mun_scores[mun].append(centered_avg)

print("municipality;ov;ob;score")
with open("overweight.csv") as f:
    for line in f.readlines():
        line_list = line.strip().split(";")
        mun = line_list[0]
        if mun in mun_scores:
            mun_scores[mun].extend([line_list[2], line_list[3]])

            print_list = [mun, str(mun_scores[mun][4]), str(mun_scores[mun][5]), str(mun_scores[mun][3])]
            print(";".join(print_list))

