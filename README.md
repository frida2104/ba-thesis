## Predicting percentages of being overweight and obesity in the Netherlands, using Dutch tweets

This repository contains all files and data used for my bachelor thesis of Information Science. 

The _tweets_ directory contains the tweets for each month in 2016, which are already filtered. __tweets_all.txt__ contains all filtered tweets of 2016. 

The _python_ directory contains all python files and auxiliary files. 

__filter_all.py__ filters all tweets (no retweets) containing a relevant word

__score.py__ calculates scores for each municipality based on particular words in tweets

__mun_scores.csv__ contains the percentages of overweight and obesity, and the scores calculated by score.py

__feature.py__ creates a python dictionary containing features for machine learning, and saves it to __features.txt__

__learn.py__ trains machine learning models and predicts percentages of being overweight and obesity
